-
title: Scaling My Kubernetes Deployment
date: 2018-10-02
tags: ["kubernetes", "code"]
---
//[...]
    NAME                                   READY     STATUS    RESTARTS   AGE       IP           NODE
    kubernetes-bootcamp-5c69669756-9jhz9   1/1  Running  0          3s 172.18.0.7   minikube
    kubernetes-bootcamp-5c69669756-lrjwz   1/1  Running   0          3s 172.18.0.5   minikube
    kubernetes-bootcamp-5c69669756-slht6   1/1  Running   0          3s 172.18.0.6   minikube
    kubernetes-bootcamp-5c69669756-t4pcs   1/1  Running   0          28s 172.18.0.4   minikube
'''